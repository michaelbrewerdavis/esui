# es-ui

This is a little elasticsearch ui.

Start with

```bash
yarn dev
```

Open [http://localhost:3003](http://localhost:3003) to start playing.

## Configuration

Elasticsearch queries are proxied through the server.

- If running on localhost, specify your username/password in the "Elasticsearch URL" field. E.g., "https://admin:admin@localhost:9200"

- If running against AWS, you can run the server with AWS credentials loaded in the environment

  ```bash
  AWS_REGION=us-east-1 aws-vault exec [env] -- yarn dev
  ```

  and specify the AWS URL in the "Elasticsearch URL" field.

## More

- The app is built on [Next.js](https://nextjs.org/docs)
