import { useCallback } from "react";
import usePersistentValue from "./usePersistentValue";

function updateHistory(
  history: string[],
  item: string,
  limit: number
): string[] {
  const index = history.indexOf(item);
  if (index >= 0) {
    return [item, ...history.slice(0, index), ...history.slice(index + 1)];
  } else {
    return [item, ...history].slice(0, limit);
  }
}

export default function useHistory(limit: number = 20) {
  const [history, setHistory] = usePersistentValue("esui_history", []);
  const addToHistory = useCallback(
    (item: string) => {
      setHistory((oldHistory: string[]) =>
        updateHistory(oldHistory, item, limit)
      );
    },
    [setHistory, limit]
  );
  return { history, addToHistory };
}
