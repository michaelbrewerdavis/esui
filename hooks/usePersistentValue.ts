import { useEffect, useState } from "react";

export default function usePersistentValue(
  key: string,
  defaultValue: any = null
) {
  const [value, setValue] = useState(defaultValue);

  useEffect(() => {
    const storedValue = window.localStorage.getItem(key);
    if (storedValue) {
      setValue(JSON.parse(storedValue));
    } else {
      setValue(defaultValue);
    }
  }, []);

  useEffect(() => {
    if (value) {
      window.localStorage.setItem(key, JSON.stringify(value));
    } else {
      window.localStorage.removeItem(key);
    }
  }, [key, value]);

  return [value, setValue];
}
