import { Flex, FormControl, FormLabel, Input, Select } from "@chakra-ui/react";
import React, { useState } from "react";

import { Config } from "../lib/Config";
import { aliases } from "../lib/elasticsearch";
import { useCallback } from "react";
import { useEffect } from "react";

export default function ConfigComponent({
  config,
  setConfig
}: {
  config: any;
  setConfig: (arg: any) => void;
}) {
  const [allIndexes, setAllIndexes] = useState<string[]>();
  const [url, setURL] = useState(config.url);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function getAllIndexes() {
      const response = await aliases(config.url);
      console.log(JSON.parse(response));
      try {
        setAllIndexes(
          JSON.parse(response)
            .map((entry: any) => entry.alias)
            .filter((alias: string) => !alias.startsWith("."))
        );
      } catch (error) {
        console.log(error);
        setError(error);
      }
    }
    if (config.url) {
      setError(null);
      getAllIndexes();
    }
  }, [config.url]);

  useEffect(() => {
    setURL(config.url);
  }, [config.url]);

  const updateConfig = useCallback(
    (updates: Partial<Config>) => {
      setConfig((config: Config) => ({ ...config, ...updates }));
    },
    [setConfig]
  );

  const onURLChange = (url: string) => {
    if (url !== config.url) {
      setAllIndexes([]);
      updateConfig({ url });
    }
  };

  console.log({ url, config });

  return (
    <Flex>
      <FormControl padding="2">
        <FormLabel>Elasticsearch URL:</FormLabel>
        <Input
          minWidth="20rem"
          placeholder="Elasticsearch URL"
          value={url || config.url}
          onChange={event => setURL(event.target.value)}
          onBlur={() => onURLChange(url)}
          size="md"
        />
      </FormControl>
      <FormControl padding="2">
        <FormLabel>Index:</FormLabel>
        <Select
          placeholder={
            allIndexes && allIndexes.length > 0
              ? "Select index..."
              : error
              ? "Error loading indexes"
              : "No indexes loaded"
          }
          value={config.index}
          onChange={event => updateConfig({ index: event.target.value })}
        >
          {allIndexes?.map(indexName => (
            <option key={indexName} value={indexName}>
              {indexName}
            </option>
          ))}
        </Select>
      </FormControl>
    </Flex>
  );
}
