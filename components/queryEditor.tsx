import { BiBracket, BiPlay } from "react-icons/bi";
import { Box, IconButton } from "@chakra-ui/react";
import React, { useCallback, useRef, useState } from "react";

import { Ace } from "ace-builds";
import AceEditor from "./AceEditor";
import ReactAce from "react-ace/lib/ace";
import usePersistentValue from "../hooks/usePersistentValue";

export default function QueryEditor({
  query,
  setQuery,
  runQuery
}: {
  query: string;
  setQuery: (query: string) => void;
  runQuery: (query: string) => void;
}) {
  const latestRunQuery = useRef(runQuery);
  latestRunQuery.current = runQuery;

  const format = useCallback(() => {
    try {
      console.log("formatting");
      const formatted = JSON.stringify(JSON.parse(query), null, "  ");
      setQuery(formatted);
    } catch (error) {}
  }, [query, setQuery]);
  const latestFormat = useRef(format);
  latestFormat.current = format;

  return (
    <AceEditor
      name="query-editor"
      value={query}
      onChange={setQuery}
      height="60vh"
      width="100%"
      mode="json"
      placeholder="Add query..."
      showPrintMargin={false}
      tabSize={2}
      commands={[
        {
          name: "send",
          bindKey: { win: "Ctrl-Enter", mac: "Cmd-Enter" },
          exec: function (editor: Ace.Editor) {
            latestRunQuery.current(editor.getValue());
          }
        },
        {
          name: "format",
          bindKey: { win: "Ctrl-Shift-F", mac: "Cmd-Shift-F" },
          exec: () => latestFormat.current()
        }
      ]}
      icons={[
        <IconButton
          key="play"
          title="Send Query [Cmd-Enter]"
          variant="link"
          aria-label="Send Query"
          icon={<BiPlay />}
          onClick={() => latestRunQuery.current(query)}
        />,
        <IconButton
          key="format"
          title="Format [Cmd-Shift-F]"
          variant="link"
          aria-label="Format"
          icon={<BiBracket />}
          onClick={format}
        />
      ]}
    />
  );
}
