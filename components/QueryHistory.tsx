import { Box, Heading, List, ListItem, Text } from "@chakra-ui/react";

import { useMemo } from "react";

function firstDiff(lhs: string, rhs: string) {
  for (let i = 0; i < Math.min(lhs.length, rhs.length); ++i) {
    if (lhs[i] !== rhs[i]) {
      return i;
    }
  }
  return Math.min(lhs.length, rhs.length);
}

function lastDiff(lhs: string, rhs: string) {
  for (let i = 0; i < Math.min(lhs.length, rhs.length); ++i) {
    if (lhs[lhs.length - i - 1] !== rhs[rhs.length - i - 1]) {
      return i;
    }
  }
  return Math.min(lhs.length, rhs.length);
}

function getDiffs(lines: string[]) {
  return lines.map((line, index) => {
    if (index === 0) {
      return line;
    }
    const previous = lines[index - 1];
    const diffStart = Math.max(0, firstDiff(previous, line) - 10);
    const diffEnd = Math.max(0, lastDiff(previous, line) - 10);
    return "..." + line.slice(diffStart, line.length - diffEnd) + "...";
  });
}

export default function QueryHistory({
  history,
  setQuery
}: {
  history: string[];
  setQuery: (query: string) => void;
}) {
  const diffs = useMemo(() => getDiffs(history), [history]);
  return (
    <Box>
      <Heading size="sm">History</Heading>
      <List>
        {history.map((query, index) => (
          <ListItem
            fontSize="xs"
            whiteSpace="nowrap"
            overflowX="scroll"
            key={query}
            onClick={() => setQuery(query)}
          >
            {index + 1}. {diffs[index]}
          </ListItem>
        ))}
      </List>
    </Box>
  );
}
