import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Box,
  Button,
  Checkbox,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
  Flex,
  Input,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useDisclosure
} from "@chakra-ui/react";
import React, { useCallback, useMemo, useState } from "react";

import AceEditor from "./AceEditor";
import { Config } from "../lib/Config";
import { explain } from "../lib/elasticsearch";
import jq from "jq-web";
import { useEffect } from "react";
import usePersistentValue from "../hooks/usePersistentValue";

function RawResults({ results }: { results: any }) {
  return (
    <AceEditor
      name="raw-results"
      height="60vh"
      width="100%"
      mode="json"
      showPrintMargin={false}
      tabSize={2}
      value={results ? JSON.stringify(results, null, "  ") : ""}
      readOnly
    />
  );
}

function PrettyFieldsCheckbox({
  field,
  value,
  prefix = "",
  selectedFields = [],
  setSelectedFields
}: {
  field: string;
  value: any;
  prefix: string;
  selectedFields: string[];
  setSelectedFields: (fields: string[]) => void;
}) {
  const prefixedField = `${prefix}${field}`;
  const selected = selectedFields.includes(prefixedField);
  const onChange = (event: any) => {
    if (event.target.checked) {
      setSelectedFields([...selectedFields, prefixedField]);
    } else {
      setSelectedFields(selectedFields.filter(f => f !== prefixedField));
    }
  };
  const newPrefix = `${prefixedField}.`;
  return (
    <>
      <Checkbox isChecked={selected} onChange={onChange}>
        {field}
      </Checkbox>
      {typeof value === "object" && selected && (
        <PrettyFieldsCheckboxes
          template={Array.isArray(value) ? value[0] : value}
          selectedFields={selectedFields}
          setSelectedFields={setSelectedFields}
          prefix={newPrefix}
        />
      )}
    </>
  );
}
function PrettyFieldsCheckboxes({
  template,
  prefix = "",
  selectedFields = [],
  setSelectedFields
}: {
  template: any;
  prefix?: string;
  selectedFields: string[];
  setSelectedFields: (fields: string[]) => void;
}) {
  const keys = template ? Object.keys(template) : [];
  return (
    <Flex direction="column" paddingLeft="2rem">
      {keys.map(f => (
        <PrettyFieldsCheckbox
          key={f}
          field={f}
          value={template[f]}
          prefix={prefix}
          selectedFields={selectedFields}
          setSelectedFields={setSelectedFields}
        />
      ))}
    </Flex>
  );
}

function PrettyFieldsModal({
  isOpen,
  onClose,
  template,
  fields,
  setFields
}: {
  isOpen: boolean;
  onClose: () => void;
  template: any;
  fields: string[];
  setFields: (fields: string[]) => void;
}) {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />
        <ModalBody paddingTop="2rem">
          <PrettyFieldsCheckboxes
            template={template || {}}
            selectedFields={fields}
            setSelectedFields={setFields}
          />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}

function resolveField(value: any, field: string): any {
  const fields = field.split(".");
  let lastValue = value;
  fields.forEach(f => {
    lastValue = lastValue[f];
  });
  return typeof lastValue === "object" ? null : lastValue;
}

function PrettyResult({
  result,
  explain,
  fields
}: {
  result: any;
  explain: (id: string) => void;
  fields: string[];
}) {
  return (
    <Accordion allowToggle width="100%">
      <AccordionItem>
        <Flex direction="column" width="100%" alignItems="flex-start">
          <Flex justifyContent="space-between" width="100%">
            <Box textAlign="start" fontWeight="bold">
              {result._source.title}
            </Box>
            <Button
              variant="link"
              onClick={event => {
                explain(result._id);
                event.stopPropagation();
              }}
            >
              {result._score}
            </Button>
          </Flex>
          <AccordionButton>
            <Box fontStyle="italic" fontSize="sm">
              {result._id}
            </Box>
            {fields.map(f => {
              const value = resolveField(result, f) ?? null;
              return value !== null ? (
                <Box fontSize="xs">
                  {f}: {value.toString()}
                </Box>
              ) : null;
            })}
          </AccordionButton>
        </Flex>
        <AccordionPanel>
          <AceEditor
            name={`result-${result._id}`}
            width="100%"
            height="10rem"
            mode="json"
            value={JSON.stringify(result, null, "  ")}
          />
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
}
function PrettyResults({
  results,
  explain
}: {
  results: any;
  explain: (id: string) => void;
}) {
  const {
    isOpen: fieldsIsOpen,
    onOpen: onOpenFields,
    onClose: onCloseFields
  } = useDisclosure();
  const [fields, setFields] = useState<any>([]);

  if (!results) {
    return null;
  }
  return (
    <Box>
      <Flex
        alignItems="center"
        justifyContent="space-between"
        padding="0 0 0.5em"
      >
        <Box>
          {results.hits.hits.length} of {results.hits.total.value} Results
        </Box>
        <Button size="sm" onClick={onOpenFields}>
          Fields
        </Button>
      </Flex>
      {results.hits.hits.map((hit: any, index: number) => (
        <Box key={hit._id}>
          <Flex direction="row" width="100%" alignItems="flex-start">
            <Box sx={{ paddingTop: 3, paddingRight: 2 }} fontSize="xs">
              {index + 1}.
            </Box>
            <PrettyResult
              key={hit._id}
              result={hit}
              explain={explain}
              fields={fields}
            />
          </Flex>
        </Box>
      ))}
      <PrettyFieldsModal
        isOpen={fieldsIsOpen}
        onClose={onCloseFields}
        template={results.hits.hits[0]}
        fields={fields}
        setFields={setFields}
      />
    </Box>
  );
}

function FilteredResults({ results }: { results: any }) {
  const lastresults = React.useRef(results);
  const [filter, setFilter] = usePersistentValue(
    "esui.jq.filter",
    ".hits.hits[]._source"
  );
  const handleChange = (event: any) => setFilter(event.target.value);

  const { filteredResults, error } = useMemo(() => {
    try {
      return { filteredResults: jq.json(results, filter) };
    } catch (error) {
      return { error };
    }
  }, [results, filter]);

  lastresults.current = filteredResults;

  return (
    <Box>
      <Flex justifyContent="flex-end">
        <Link
          fontSize="xs"
          target="_blank"
          href="https://cheatography.com/orabig/cheat-sheets/jq/"
        >
          jq cheatsheet
        </Link>
      </Flex>
      <Input
        value={filter}
        onChange={handleChange}
        placeholder="jq syntax query"
      />
      <AceEditor
        name="filtered_results"
        height="90vh"
        width="100%"
        mode="json"
        showPrintMargin={false}
        tabSize={2}
        value={JSON.stringify(filteredResults ?? error, null, "  ")}
        readOnly
      />
    </Box>
  );
}

function ExplainResult({
  documentId,
  setDocumentId,
  config,
  body
}: {
  documentId?: string;
  setDocumentId: (arg: string) => void;
  config: Config;
  body: any;
}) {
  const [explanation, setExplanation] = useState(null);

  useEffect(() => {
    async function loadExplanation() {
      const explainBody = body.rescore
        ? { query: body.rescore.query.rescore_query }
        : { query: body.query };
      try {
        const reason = await explain(
          config.url,
          config.index,
          explainBody,
          documentId as string
        );
        setExplanation(JSON.parse(reason));
      } catch (error) {
        console.log(error);
        setExplanation(error);
      }
    }
    setExplanation(null);
    if (documentId && body && config) {
      loadExplanation();
    }
  }, [documentId, body, config, setExplanation]);

  return (
    <Box>
      <Input
        value={documentId}
        placeholder="Document ID"
        onChange={event => setDocumentId(event.target.value)}
      />
      <AceEditor
        name="explain_result"
        height="90vh"
        width="100%"
        mode="json"
        showPrintMargin={false}
        tabSize={2}
        value={explanation ? JSON.stringify(explanation, null, "  ") : ""}
        readOnly
      />
    </Box>
  );
}

export default function QueryResults({
  config,
  body,
  results
}: {
  results: any;
  config: Config;
  body: any;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const [explainId, setExplainId] = useState<string>();
  const explain = useCallback(
    (documentId: string) => {
      setExplainId(documentId);
      setTabIndex(3);
    },
    [setExplainId, setTabIndex]
  );

  return (
    <Tabs isLazy onChange={index => setTabIndex(index)} index={tabIndex}>
      <TabList>
        <Tab>Pretty</Tab>
        <Tab>Raw</Tab>
        <Tab>jq Filter</Tab>
        <Tab>Explain</Tab>
      </TabList>
      <TabPanels>
        <TabPanel>
          <PrettyResults results={results} explain={explain} />
        </TabPanel>
        <TabPanel>
          <RawResults results={results} />;
        </TabPanel>
        <TabPanel>
          <FilteredResults results={results} />
        </TabPanel>
        <TabPanel>
          <ExplainResult
            documentId={explainId}
            setDocumentId={setExplainId}
            config={config}
            body={body}
          />
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
}
