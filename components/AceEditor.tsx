import {
  Box,
  Button,
  Flex,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  useDisclosure
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";

import { BiArea } from "react-icons/bi";
import { IAceEditorProps } from "react-ace";

type EditorProps = { icons?: any[] } & IAceEditorProps;

export default function AceEditor(props: EditorProps) {
  const { icons = [], ...aceEditorProps } = props;

  const [Editor, setEditor] = useState<any>(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  useEffect(() => {
    const initialize = async () => {
      const ace = await import("ace-builds/src-noconflict/ace");
      // ensures syntax validation webworkers can function
      ace.config.set(
        "basePath",
        "https://cdn.jsdelivr.net/npm/ace-builds@1.4.3/src-noconflict/"
      );
      const ReactAce = await import("react-ace");
      await import("ace-builds/src-noconflict/mode-json");
      await import("ace-builds/src-noconflict/ext-language_tools");

      setEditor(() => ReactAce.default);
    };
    initialize();
  }, []);

  return Editor ? (
    <div>
      <Flex justifyContent="flex-end" padding="1">
        {icons}
        <IconButton
          aria-label="Expand"
          variant="link"
          title="Expand"
          icon={<BiArea />}
          onClick={onOpen}
        />
      </Flex>
      <Editor {...aceEditorProps} />
      <Modal isOpen={isOpen} onClose={onClose} size="full">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader />
          <ModalCloseButton />
          <ModalBody>
            <Box paddingTop="3" height="90vh">
              <Editor
                {...props}
                name={`${props.name}-expanded`}
                width="100%"
                height="100%"
              />
            </Box>
          </ModalBody>
        </ModalContent>
      </Modal>
    </div>
  ) : (
    <div />
  );
}
