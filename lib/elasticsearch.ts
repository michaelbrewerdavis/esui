async function makeRequest(url: string, method: string, params: any) {
  const response = await fetch("/api/es", {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      url,
      method,
      params
    })
  });
  return await response.text();
}

export async function search(
  url: string,
  index: string,
  body: any
): Promise<string> {
  return await makeRequest(url, "search", {
    index,
    body: JSON.stringify(body)
  });
}

export async function explain(
  url: string,
  index: string,
  body: any,
  id: string
): Promise<string> {
  return await makeRequest(url, "explain", {
    id,
    index,
    body: JSON.stringify(body)
  });
}

export async function aliases(url: string): Promise<string> {
  return await makeRequest(url, "aliases", { format: "json" });
}
