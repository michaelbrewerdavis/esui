import type { NextApiRequest, NextApiResponse } from "next";

import AWS from "aws-sdk";
import { Client } from "@elastic/elasticsearch";
import createAwsElasticsearchConnector from "aws-elasticsearch-connector";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  try {
    const {
      url,
      method,
      params
    }: {
      url: string;
      method: "search" | "explain" | "aliases";
      params: any;
    } = req.body;
    console.log(req.body);
    const client = url.includes("amazonaws")
      ? new Client({
          ...createAwsElasticsearchConnector(AWS.config),
          node: url
        })
      : new Client({
          node: url,
          ssl: {
            rejectUnauthorized: false
          }
        });
    const response =
      method === "aliases"
        ? await client.cat.aliases(params)
        : await client[method](params);

    console.log(response);
    console.log(JSON.stringify(response, null, "  "));
    // @ts-ignore
    res.status(200).json(response.body);
  } catch (error) {
    console.log(error);
    res.status(500).json(JSON.stringify({ error }));
  }
}
