import { Alert, Box, Flex } from "@chakra-ui/react";
import { useCallback, useEffect, useRef, useState } from "react";

import { Client } from "@elastic/elasticsearch";
import Config from "../components/config";
import Head from "next/head";
import QueryEditor from "../components/queryEditor";
import QueryHistory from "../components/QueryHistory";
import QueryResults from "../components/queryResults";
import { search } from "../lib/elasticsearch";
import useHistory from "../hooks/useHistory";
import usePersistentValue from "../hooks/usePersistentValue";

export default function Home() {
  const { history, addToHistory } = useHistory();

  const [config, setConfig] = usePersistentValue("esui_config", {});
  const [query, setQuery] = usePersistentValue("esui_query", "");
  const [lastQuery, setLastQuery] = useState<any>(null);
  const [results, setResults] = useState<any>(null);
  const [error, setError] = useState<any>(null);

  const runQuery = useCallback(
    async queryBody => {
      try {
        const results = await search(
          config.url,
          config.index,
          JSON.parse(queryBody)
        );
        try {
          const parsed = JSON.parse(results);
          if (parsed.error) {
            setError(parsed.error);
          } else {
            setLastQuery(JSON.parse(queryBody));
            setResults(parsed);
            setError(null);
            addToHistory(queryBody);
          }
        } catch (parseError) {
          setError({ parseError, results });
        }
      } catch (queryError) {
        console.log(queryError);
        setError(queryError.toString());
      }
    },
    [config, setResults, addToHistory]
  );

  return (
    <Flex direction="column" alignContent="stretch">
      <Head>
        <title>es-ui</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Flex direction="row" justifyContent="flex-end">
        <Box>
          <Config config={config} setConfig={setConfig} />
        </Box>
      </Flex>

      <Flex direction="row" padding="0.5rem">
        <Box
          width="50%"
          sx={{ flexBasis: 0, flexGrow: 1, paddingRight: "0.5rem" }}
        >
          <QueryEditor query={query} setQuery={setQuery} runQuery={runQuery} />
          <QueryHistory history={history} setQuery={setQuery} />
        </Box>
        <Box width="50%" sx={{ flexBasis: 0, flexGrow: 1 }}>
          {error && <Alert status="error">{JSON.stringify(error)}</Alert>}
          <QueryResults results={results} config={config} body={lastQuery} />
        </Box>
      </Flex>
    </Flex>
  );
}
