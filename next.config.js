module.exports = {
  reactStrictMode: true,
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // needed for jq-web
    // https://github.com/fiatjaf/jq-web/issues/5#issuecomment-854231848
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }
    return config;
  }
};
